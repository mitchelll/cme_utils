import pytest

def test_imports_no_hoomd():
    import cme_utils
    import cme_utils.plot
    import cme_utils.job_control
    import cme_utils.manip
    import cme_utils.analyze
    from cme_utils.analyze import autocorr,diffractometer,diffract,peakfinder
    from cme_utils.plot import logplot

def test_import_hoomd():
    with pytest.raises(ImportError):
        from cme_utils.manip.convert_rigid import init_wrapper
