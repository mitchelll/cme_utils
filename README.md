Python repository for useful helper functions used by the Jankowski lab.

## Install Instructions ##

```
#!bash

git clone git@bitbucket.org:cmelab/cme_utils.git
cd cme_utils
pip install -e .
```

Known Issues:

1. MDTraj has an issue with cython sometimes, `pip install cython` fixes this issues.

1. [hoomd-blue](http://glotzerlab.engin.umich.edu/hoomd-blue/) needs to be installed to get full functionality
