import gsd
import gsd.fl
import gsd.hoomd
import argparse

def SliceGSD(input_file,output_file,start_frame,end_frame,step):
    f = gsd.fl.open(name=output_file,
                    mode='wb+',
                    application="Epoxpy",
                    schema="My Schema",
                    schema_version=[1,0])
    f.close()
    t_out = gsd.hoomd.open(name=output_file, mode='wb')
    f = gsd.fl.GSDFile(input_file, 'rb')
    t_in = gsd.hoomd.HOOMDTrajectory(f)
    for index in range(start_frame,end_frame,step):
        t_out.append(t_in[index])

def main():
    parser = argparse.ArgumentParser(description='Thins down a gsd file so that the vmd plug in can load it.' )
    parser.add_argument("-i","--input_gsd", help="The path to the gsd file that needs to be thinned",
                        type=str,
                        default='')
    parser.add_argument("-o","--output_gsd",
                        help="The path to the thinned gsd file",
                        type=str,
                        default='thin.gsd')
    parser.add_argument("-s","--stride",
                        help="The number of frames to between the thinned frames",
                        type=int,
                        default=-1)
    args = parser.parse_args()
    if args.input_gsd is '':
        print("Please provide an input gsd path using the -i option")
        return
    if args.output_gsd is 'thin.gsd':
        print("The thinned gsd will be saved as thin.gsd in the working directory")
    print('Input gsd:',args.input_gsd)
    print('Output gsd:',args.output_gsd)
    f = gsd.fl.GSDFile(args.input_gsd, 'rb')
    t_in = gsd.hoomd.HOOMDTrajectory(f)
    if args.stride==-1:
        print("-stride was not specified. Using frame count/10")
        args.stride = int(len(t_in)/10)

    SliceGSD(args.input_gsd,args.output_gsd,0,len(t_in)-1,args.stride)
