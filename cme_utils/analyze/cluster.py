#!/Users/evanmiller/anaconda/bin/python
"""
The purpose of this script is to calculate the number of clusters that exist in a simulation.
"""

#Import the module files
from sys import argv
import numpy as np
import mdtraj as md
from mpl_toolkits.mplot3d import Axes3D

#Get values from trajectory and topologies
TRAJ_FILE = argv[2]
TOP_FILE = argv[1]
t = md.load(TRAJ_FILE, top = TOP_FILE)

"Can be used if you want to visualize the average points."
#fig=plt.figure()
#ax= fig.add_subplot(111, projection='3d')

#These lines are for the different variables to use in loops.
frames = (t.n_frames-1)#gets the final frame

#Set the number of atoms in each molecule
atoms_per_molecule = 20
#Print statement to help with verification and reminder to switch.
#print "There are %s atoms per molecule." %atoms_per_molecule

#Calculate the number of molecules based off of the total number
#of atoms and atoms per molecule.
n_mol = int(t.n_atoms/atoms_per_molecule)

#Get the length of the unit cell edges.
axes = t.unitcell_lengths[t.n_frames-1]

dist = 0.334# The distance to determine if two molecules are in the same cluster. 
#NOTE:Units are nm from md.traj, the desired cut off distance was detmined by 
#using a dummy system that produced logical results.

#Dot product cut off to be considered in the same cluster.
dot_cut = 0.92

#Setting up the empty lists.
#Lists to place points used for the averages in cm_list.
p1_list = []
p2_list = []
p3_list = []
cm_list = []#An empty list for the average positions/center of mass
n_list = [ [] for i in range(n_mol) ]#List for the neighbors.
cp_list = [] #for the cross product.

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def average_position(p1, p2, p3):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    p1 = p3 - v1
    p2 = p3 - v2
    average = (p1+p2+p3) / 3.
    return average

def cross_product(p1, p2, p3):
    """Calculates the cross product from three members of a ring"""
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    cp = np.cross(v1, v2)
    cp /= np.linalg.norm(cp)
    return cp

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    print("calling function", particle)
    for n in neighbor_list[particle]:
        print("checking neighbor", particle,":", n)
        if cluster_list[n]>cluster_list[particle]:
            print("updating", n,"to" ,cluster_list[particle])
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            print("Updating", cluster_list[particle], "to", cluster_list[n])
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

"""The indices over which you want to loop."""
p1_list = t.xyz[frames,4:-1:atoms_per_molecule,:]
p2_list = t.xyz[frames,8:-1:atoms_per_molecule,:]
p3_list = t.xyz[frames,11:-1:atoms_per_molecule,:]
for i in range(n_mol):
    cm_list.append(average_position(p1_list[i], p2_list[i], p3_list[i]))
    cp_list.append(cross_product(p1_list[i], p2_list[i], p3_list[i]))

#convert the list of arrays into an array of arrays.
cm_list = np.array(cm_list)
cp_list = np.array(cp_list)

#Build the neighborlist
for i in range(0,len(cm_list)-1):
    for j in range(i+1,len(cm_list)):
        a = cm_list[i,:]
        b = cm_list[j,:]
        d = np.linalg.norm(pbc(b-a))
        if  d < dist :
            cp1 = cp_list[i,:]
            cp2 = cp_list[j,:]
            dot = abs(np.dot(cp1, cp2))
            if dot > dot_cut: 
                if i < j:
                    n_list[i].append(j)
                
for i,neighbors in enumerate(n_list):
    print(i, neighbors)

c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = update_neighbors(i,c_list,n_list)

"""Show which residues are in which clusters."""
#output
for i in range(len(n_list)):
    inclust = ""
    for j,c in enumerate(c_list):
        if c==i:
            inclust+=str(j)+" "
    if inclust !="":
        print(i,inclust)
#print len(set(c_list)), "clusters"

"""Determine the overall order of the system"""
order_list = []
for i in range(0, np.amax(c_list)+1):
#    if c_list.count(i) != 0:
#        print "Cluster", i, "has", c_list.count(i), "members"
    order = float(c_list.count(i))/float(len(c_list)) 
    if order >= 0.15:#Chosen from a dummy system.
        order_list.append(order)
#        print order
#print np.sum(order_list)
final_order =  np.sum(order_list)
print(final_order)

#del p1_list, p2_list, p3_list, cm_list, n_list, cp_list, order_list
#'''Setting up loop to do this over multiple frames'''
#ovf = []
#frame_order = 0.0
#for current in range(1,frames):
#    #if abs(frame_order-final_order) > 0.1 and abs(frame_order-last) < 0.15:
#    p1_list = []
#    p2_list = []
#    p3_list = []
#    cm_list = []#An empty list for the average positions/center of mass
#    n_list = [ [] for i in range(n_mol) ]#List for the neighbors.
#    cp_list = [] #for the cross product.
#    """The indices overwhich you want to loop."""
#    p1_list = t.xyz[current,4:-1:atoms_per_molecule,:]
#    p2_list = t.xyz[current,8:-1:atoms_per_molecule,:]
#    p3_list = t.xyz[current,11:-1:atoms_per_molecule,:]
#    for i in range(n_mol):
#        cm_list.append(average_position(p1_list[i], p2_list[i], p3_list[i]))
#        cp_list.append(cross_product(p1_list[i], p2_list[i], p3_list[i]))
#
#    #convert the list of arrays into an array of arrays.
#    cm_list = np.array(cm_list)
#    cp_list = np.array(cp_list)
#
#    #Build the neighborlist
#    for i in range(0,len(cm_list)-1):
#        for j in range(i+1,len(cm_list)):
#            a = cm_list[i,:]
#            b = cm_list[j,:]
#            d = np.linalg.norm(pbc(b-a))
#            if  d < dist :
#                cp1 = cp_list[i,:]
#                cp2 = cp_list[j,:]
#                dot = abs(np.dot(cp1, cp2))
#                if dot > dot_cut: 
#                    """Test statements"""
#                    #print "found ", i, j, d,
#                    #print "and dot", dot
#                    #print a , b ,' dist: ', d
#                    #print "joining", i,j
#                    n_list[i].append(j)
#                    n_list[j].append(i)
#                    #print cm_list[i,:], cm_list[j,:], np.linalg.norm(pbc(cm_list[i,:]-cm_list[j,:]))
#
#    """Converts items from the neighborlist into a cluster list."""
#    c_list=[i for i in range(len(n_list))]
#    #print c_list
#    for i,nbers in enumerate(n_list):
#        for n in nbers:
#            if i < n:
#                c_list[n]=c_list[i]
#
#    """Show which residues are in which clusters."""
#    #output
#    for i in range(len(n_list)):
#        inclust = ""
#        for j,c in enumerate(c_list):
#            if c==i:
#                inclust+=str(j)+" "
#    #    if inclust !="":
#    #        print i,inclust
#    #print len(set(c_list)), "clusters"
#
#    """Determine the overall order of the system"""
#    order_list = []
#    for i in range(0, np.amax(c_list)+1):
#    #    if c_list.count(i) != 0:
#    #        print "Cluster", i, "has", c_list.count(i), "members"
#        order = float(c_list.count(i))/float(len(c_list)) 
#        if order >= 0.15:#Chosen from a dummy system.
#            order_list.append(order)
#    #        print order
#    #print np.sum(order_list)
#    frame_order = np.sum(order_list)
##    print current
#    ovf.append(frame_order)
#    del p1_list, p2_list, p3_list, cm_list, n_list, cp_list, order_list
#
#ovf = np.array(ovf)
##print ovf
#for i in range(len(ovf)):
#    print i, ovf[i]
##for i in range(len(ovf)-1):
##    if abs(ovf[-1]-ovf[i]) < 0.1 and abs(ovf[i]-ovf[i+1]) < 0.1:
##        print  i
##        break
