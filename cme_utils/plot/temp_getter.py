from __future__ import print_function
from __future__ import division
from builtins import str
from builtins import range
from past.utils import old_div
import matplotlib.pyplot as plt
import numpy as np
import glob
import re
import matplotlib.cm as cm
import sys
import help_functions as hlp
if __name__ == "__main__":



    x_tmp = []
    y_tmp = []
    yerr_tmp = []
    auto_coor_time = []
    y_loc_tmp = []
    y_loc_err_tmp = []
    list_of_directories = [vals for vals in sys.argv[1:]]


    num_plots = len(list_of_directories)
    y_array = []
    y_std = []
    for directory in list_of_directories:
        try:
            x = float((re.search("T(\d\.\d)", directory).groups()[0]))
        except (AttributeError):
            x = float((re.search("T(\d\d\.\d)", directory).groups()[0]))
        run_dir = directory#[:-9]
        window = np.genfromtxt(run_dir+"LJ_window.txt", delimiter=" ")
        print(window)
        y_a1 = window[0] #4 #old_div(int(window[0]),10.0)
        y_s1 = window[1]#5
        y_s2 = window[2]

        auto_coor_time.append(y_s2)
        x_tmp.append(x)
        y_tmp.append(y_a1)
        yerr_tmp.append(y_s1)
    #print((x_tmp, y_tmp, yerr_tmp))
    with open("auto-cor-data-"+list_of_directories[0].split("/")[-2], "w") as text_file:
        text_file.write("#temp start_frame num_samples auto_corr_time\n")
        for _ in range(len(x_tmp)):
            text_file.write("{} {} {} {}\n".format(x_tmp[_], y_tmp[_], yerr_tmp[_], auto_coor_time[_]))

    #plt.plot(x_tmp, y_tmp, marker='o', linewidth=0)
    #plt.plot(x_tmp,np.full((len(x_tmp),1),1e5))
    #plt.plot(x_tmp,np.full((len(x_tmp),1),1e6))
    #plt.yscale('log', nonposy='clip')
    #plt.show()
