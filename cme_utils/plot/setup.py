from os.path import join
from distutils.core import setup
PACKAGES = ['cme_utils',
            'cme_utils.analyze',
            'cme_utils.plot',
            'cme_utils.manip'
            ]
PACKAGE_DIR = {}
PACKAGE_DATA={}
for pkg in PACKAGES:
    PACKAGE_DIR[pkg] = join(*pkg.split('.'))
setup(
    name='cme_utils',
    version = '0.1',
    author = 'Eric Jankowski',
    author_email = 'ericjankowski@boisestate.edu',
    url = 'https://bitbucket.org/cmelab/cme_utils',
    description='Jankowski lab helper functions.',
    packages=PACKAGES,
    package_dir=PACKAGE_DIR,
	package_data=PACKAGE_DATA
)

